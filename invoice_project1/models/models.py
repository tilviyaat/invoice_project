# -*- coding: utf-8 -*-
import datetime
from odoo import models, fields, api,_,modules
from pytz import timezone, UTC
from datetime import date



class Limits(models.Model):
    _inherit = 'res.users'
    # box = fields.Many2many('res.branch','name',string='Grant Permission')
    # permitted_branch =fields.Char(string='Permitted Branches')


    users_limit = fields.Float(string='User limit')

    # @api.onchange('branch_id')
    # def _onchange_limit(self):
    #     branch = self.env['res.branch'].search([('id', '=', self.branch_id.id)])
    #     print(branch,'branch is')

    #     # for result in results:
    #     self.update(
    #         {
    #             'users_limit': branch.users_limit
    #         }
    #
    #     )
    #     if branch.users_limit != 0.00:
    #         self.update({
    #
    #             'box': True
    #
    #         })
    #     else:
    #         self.update({
    #
    #             'box': False

            # })


class Branch(models.Model):
    _inherit = "res.branch"
    branch_limit = fields.Float(string='Branch Limit')
    permission = fields.Boolean(string='Permission')
    @api.onchange('branch_limit')
    def default(self):
        user_list = self.env['res.users'].search([("branch_id.name", "=", self.name)])
        for i in user_list:
            # print(i.branch_limit)
            # self.branch_id.branch_limit = i.branch_limit
            i.users_limit = self.branch_limit


    # @api.onchange('branch_limit')
    # def _onchange_limit(self):
    #
    #     limit = self.branch_limit
    #
    #     user = self.env['res.users'].search([('branch_id.name', '=', self.name)])
    #     print('hyj',user)
        # user.update({

    #         'cost': limit
    #
    #     })
    #     if limit != 0.00:
    #         user.update({
    #
    #             'box': True
    #
    #         })
    #     else:
    #         user.update({

    #
    #             'box': False
    #
    #         })


class Schedule(models.Model):
    _inherit = "mail.activity.type"
    category = fields.Selection(selection_add=[
        ('approve', 'Approved'),
        ('reject', 'Rejected')
    ])


class ScheduleActivity(models.Model):
    _inherit = "mail.activity"
    activity_manager = fields.Boolean(compute='compute_manager')

    def schedule_reject(self):
        obj = self.env[self.res_model].search([('id', '=', self.res_id)])
        obj.state = "rejected"

    def schedule_approval(self):
        obj = self.env[self.res_model].search([('id', '=', self.res_id)])
        obj.state = "approved"


    def compute_manager(self):

        for i in self:
            obj = self.env['account.move'].search([('id', '=', i.res_id)])
            print(obj.manager)

            if obj.manager == True:
                self.activity_manager = True

            else:
                self.activity_manager = False
class Invoice_Notification(models.Model):
    name = 'res.users'
    _inherit = ['res.users']

    @api.onchange('branch_id')
    def default(self):

        branch_val = self.env['res.branch'].search([("name", "=", self.branch_id.name)])

        for i in branch_val:
            print(i.branch_limit)
            self.branch_id.branch_limit = i.branch_limit
            self.users_limit = i.branch_limit


    @api.model
    def systray_get_activities(self):
        # new = self.env['mail.activity'].search(
        #     [('activity_type_id', '=', 'Approval'), ('user_id', '=', self.env.user.id)])
        res = super(Invoice_Notification, self).systray_get_activities()
        query = """SELECT m.id, count(*), act.res_model as model,
                                      CASE
                                          WHEN %(today)s::date - act.date_deadline::date = 0 Then 'today'
                                          WHEN %(today)s::date - act.date_deadline::date > 0 Then 'overdue'
                                          WHEN %(today)s::date - act.date_deadline::date < 0 Then 'planned'
                                      END AS states
                                  FROM mail_activity AS act
                                  JOIN ir_model AS m ON act.res_model_id = m.id
                                  WHERE user_id = %(user_id)s
                                  GROUP BY m.id, states, act.res_model;
                                  """
        self.env.cr.execute(query, {
            'today': fields.Date.context_today(self),
            'user_id': self.env.uid,
        })

        activity_data = self.env.cr.dictfetchall()
        print('activity_data',activity_data)
        model_ids = [a['id'] for a in activity_data]
        model_names = {n[0]: n[1] for n in self.env['ir.model'].browse(model_ids).name_get()}
        print('modelnames',model_names)
        new = self.env['mail.activity'].search(
            [('activity_type_id', '=', 'Approval'), ('user_id', '=', self.env.user.id)])
        today1 = 0
        overdue1 = 0
        planned1 = 0
        today = date.today()

        for sample in new:
            if sample.date_deadline == today:
                today1 = today1 + 1
            elif sample.date_deadline < today:
                overdue1 = overdue1 + 1
            else:
                planned1 = planned1 + 1

        overall = today1+overdue1
        print(overall)


        user_activities = {
            'type': 'activity',
            'name': 'Invoice Project',
            'model': 'invoice.approval',
            'icon': modules.module.get_module_icon(self.env['invoice.approval']._original_module),
            'total_count': overall, 'today_count':today1 , 'overdue_count': overdue1, 'planned_count': planned1,
        }
        # activity.insert(3, user_activities)

        res.insert(3, user_activities)

        return res