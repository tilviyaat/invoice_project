from odoo import models, fields, api,_
from odoo.http import request
from openerp.exceptions import ValidationError

class Approval(models.Model):
    _name = "invoice.approval"
    activity_user_id = fields.Integer('res.users')
    id = fields.Char('id')
    name = fields.Char('name')
    type = fields.Char('type')
    color = fields.Char('color')
    show_on_dashboard = fields.Integer('show_on_dashboard')
    kanban_dashboard = fields.Integer('kanban_dashboard')
    activity_ids = fields.Integer('activity_ids')
    activity_state = fields.Integer('activity_state')

    def open_action(self):
        """return action based on type for related journals"""
        # action_name = self._context.get('action_name')
        current_manager = self.env['hr.employee'].search([("name", "=", self.env.user.name)])

        # Find action based on journal.
        if self.type == 'sale':
            action = {
                'name': _('Sale'),
                'type': 'ir.actions.act_window',
                'views': [[False,'kanban'],[False,'tree'],[False,'form']],
                'res_model': 'sale.order',
                # 'domain':['current_manager','=','manager_name']
                # 'context': ctx,
            }
            return action
        elif self.type == 'purchase':
            action = {
                'name': _('Purchase'),
                'type': 'ir.actions.act_window',
                'views': [[False, 'kanban'], [False, 'tree'], [False, 'form']],
                'res_model': 'purchase.order',
                # 'context': ctx,
            }
            return action
        elif self.type == 'account':
            action = {
                'name': _('Invoice'),
                'type': 'ir.actions.act_window',
                'views': [[False, 'kanban'], [False, 'tree'], [False, 'form']],
                'res_model': 'account.move',
                # 'context': ctx,
            }
            return action

    def action_create_new(self):
        ctx = self._context.copy()
        ctx['default_journal_id'] = self.id
        if self.type == 'sale':
            ctx['default_type'] = 'out_refund' if ctx.get('refund') else 'out_invoice'
        elif self.type == 'purchase':
            ctx['default_type'] = 'in_refund' if ctx.get('refund') else 'in_invoice'
        else:
            ctx['default_type'] = 'entry'
            ctx['view_no_maturity'] = True
        return {
            'name': _('Create invoice/bill'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'account.move',
            'view_id': self.env.ref('account.view_move_form').id,
            'context': ctx,
        }
class Invoice_approval(models.Model):
    schedule_manager = fields.Many2one('res.users', string='Schedule_Manager')
    _inherit = "account.move"
    state = fields.Selection([

        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('posted','Posted'),
        # ('rfa','Waiting for approval'),
        ('overlimit','Request for approval'),
        ('wfa','Waiting for approval')

    ], string='Status', readonly=True, default='draft')
    current_activity_id = fields.Integer()
    # reason = fields.Char(string='Reason', required=True)
    # hr_expense_ids = fields.Many2many('hr.expense')
    # hr_expense_sheet_id = fields.Many2one('hr.expense.sheet')

    def action_create_new(self):
        ctx = self._context.copy()
        ctx['default_journal_id'] = self.id
        if self.type == 'sale':
            ctx['default_type'] = 'out_refund' if ctx.get('refund') else 'out_invoice'
        elif self.type == 'purchase':
            ctx['default_type'] = 'in_refund' if ctx.get('refund') else 'in_invoice'
        else:
            ctx['default_type'] = 'entry'
            ctx['view_no_maturity'] = True
        return {
            'name': _('Create invoice/bill'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'account.move',
            'view_id': self.env.ref('account.view_move_form').id,
            'context': ctx,
        }

    def waiting_approval(self):
        self.state = 'wfa'


    def request_approve(self):
        self.state = 'approved'
        for rec in self:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (rec.id, rec._name)
            body_html = _("""Quotation %s has been approved by %s<br>
                               <p style="margin: 16px 0px 16px 0px;">
                               <a href="%s" target="_blank"
                               style="background-color:#875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px;">
                               View Quotation
                               </a></p>""") % \
                        (rec.name, rec.env.user.name, base_url)
            subtype_id = self.env['mail.message.subtype'].search([('name', '=', "Discussions")])
            subtype_log = self.env['mail.message.subtype'].search([('name', '=', "Note")])

            rec.env['mail.mail'].create({
                'body_html': body_html,
                'state': 'outgoing',
                'email_to': str(rec.user_id.partner_id.email),
                'subject': "Manager Approved"
            }).send()


    def request_confirm(self):
        current_user = self.env['res.users'].search([("name", "=", self.env.user.name)])
        total_amount = self.amount_total

        for i in current_user:
            if total_amount <= i.users_limit:
                print(i.users_limit)
                self.state = 'confirm'
            else:
                self.state = 'overlimit'

    def request_rejected(self,text_box):
        self.state = 'rejected'
        print(self)

        for rec in self:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (rec.id, rec._name)
            body_html = _("""Quotation %s has been rejected by %s<br>
                        <p style="margin: 16px 0px 16px 0px;">
                        <a href="%s" target="_blank"
                        style="background-color:#875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px;">
                        View Quotation
                        </a></p>""") % \
                         (rec.name, rec.env.user.name, base_url)
            subtype_id = self.env['mail.message.subtype'].search([('name', '=', "Discussions")])
            subtype_log = self.env['mail.message.subtype'].search([('name', '=', "Note")])
            self.env['mail.mail'].create({
                'body_html': body_html + text_box,
                'state': 'outgoing',
                'email_to': str(rec.user_id.partner_id.email),
                'subject': "Manager Rejected"
            }).send()
            for sheet in self:
                sheet.message_post_with_view('invoice_project.invoice_project_template_refuse_reason',
                  values={'text_box': text_box, 'is_sheet': True, 'name': self.name})
            # self.create_activity()

    def request_cancel(self):
        self.state = 'cancel'
        # usr_record = self.env['hr.employee'].search([('name', '=', self.env.user.name)])
        # print(usr_record.name, 'manager')
        # branch = self.env['res.branch'].search([('id', '=', self.branch_id.id)])
        # print(branch.name,'branch is ')

        # obj = self.env['mail.activity'].search([('user_id.name','=',self.env.user)])
        # print(obj.name)

    def action_post(self):
        self.state = 'posted'

    def request_approval(self, text_box):
        self.create_activity()
        in_manager = self.env['res.users'].search([('name', '=', self.env.user.name)])
        print(in_manager.name,'current user')

        print(self.manager_name)
        usr_record = self.env['hr.employee'].search([('name', '=', self.env.user.name)])
        print(usr_record.name,'manager')
        self.manager_name = usr_record.parent_id.name
        print(self.manager_name)

        hr_employee = self.env['res.users'].search([('name', '=', self.manager_name)]).users_limit
        print(hr_employee)

        if self.amount_total <= hr_employee:
            self.state = 'wfa'
        else:
            for rec in self:
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                base_url += '/x/dweb#id=%d&view_type=form&model=%s' % (rec.id, rec._name)
                body_html = _("""Quotation %s has been requested  by %s<br>
                                       <p style="margin: 16px 0px 16px 0px;">
                                       <a href="%s" target="_blank"
                                       style="background-color:#875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px;">
                                       View Quotation
                                       </a></p>""") % \
                            (rec.name, rec.env.user.name, base_url)
                self.env['mail.mail'].create({
                    'body_html': body_html+text_box,
                    'state': 'outgoing',
                    'email_to': str(usr_record.parent_id.work_email),
                    'subject': "Request for approval"
                }).send()
                for sheet in self:
                    sheet.message_post_with_view('invoice_project.request_reason',
                                                 values={'text_box': text_box, 'is_sheet': True, 'name': self.name})
                self.write({'state': 'overlimit'})

    def request_manager(self):
                current_user = self.env['res.users'].search([("name", "=", self.env.user.name)]).users_limit
                if current_user >= self.amount_total:
                    self.manager = True
                else:
                    self.manager = False
    manager = fields.Boolean(string='manager', compute='request_manager')
    manager_name = fields.Char(string='manager name')

    def create_activity(self):
        current_manager = self.env['hr.employee'].search([("name", "=", self.env.user.name)])
        self.manager_name = current_manager.parent_id.name
        hr_manager = self.env['res.users'].search([('name', '=', self.manager_name)])

        current_activity = self.activity_schedule(
            'invoice_project.mail_activity_approve',
            user_id=hr_manager.id)
        print(current_activity.id)
        self.current_activity_id = current_activity.id

    def action_approve(self):
        current_user = self.env['res.users'].search([("name", "=", self.env.user.name)]).users_limit
        print(current_user)

        if self.amount_total <= current_user:
            self.state = 'posted'

        elif len(self.env['hr.employee'].search([("id",'=',self.env.user.id)]).parent_id)==0:
            raise ValidationError(_('Sorry, undefined manager'))

        elif self.amount_total <= current_user:
            self.state = 'posted'

        else:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'request.wizard.window',
                'view_type': 'form',
                'view_mode': 'form',
                'context': self.env.context,
                'target': 'new',
            }



# class RecordRules(models.Model):
#     _inherit = 'hr.employee'
#     def _count(self):
#         if len(self.subordinate_ids) != 0:
#             for i in self.subordinate_ids:
#                 self.child = self.child + i
#         else:
#             self.child = self.child
#     child = fields.Many2many('hr.employee', 'parent_id', string='Sub Ordinates', compute='_count')
