from odoo import models, fields, api,_
class ApproveWizard(models.TransientModel):
    _name = "approve.wizard.window"
    # text_box = fields.Text(string='Reason')

    def request_approve(self):
        active_id = self.env.context.get('active_id')
        print ("active_id", active_id)
        rec = self.env['account.move'].search([('id', '=', active_id)])
        print(rec)
        rec.request_approve()
        current_id = self.env['mail.activity'].search([('id', '=', rec.current_activity_id)])
        print(current_id)
        current_id.unlink()



class RejectWizard(models.TransientModel):
    _name = "reject.wizard.window"
    text_box = fields.Text(string='Reason')

    def request_rejected(self):
        active_id = self.env.context.get('active_id')
        print("active_id", active_id)
        rec = self.env['account.move'].search([('id','=',active_id)])
        print(rec)
        rec.request_rejected(self.text_box)
        current_id = self.env['mail.activity'].search([('id', '=', rec.current_activity_id)])
        print(current_id)
        current_id.unlink()


class RequestWizard(models.TransientModel):
    _name = "request.wizard.window"
    text_box = fields.Text(string='Reason')
    partner_ids = fields.Many2many("res.users",string="recipients")

    def request_approval(self):
        active_id = self.env.context.get('active_id')
        print("active_id", active_id)
        rec = self.env['account.move'].search([('id','=',active_id)])
        print(rec)
        rec.request_approval(self.text_box)






