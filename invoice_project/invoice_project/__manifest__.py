# -*- coding: utf-8 -*-
{
    'name': "invoice_project",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','branch','account','mail'],

    # always loaded
    'data': [
        # 'security/ir.model.csv',
        'security/group.xml',
        'views/branch_settings.xml',
        'wizard/approve_wizard.xml',
        'views/statusbar.xml',
        'views/manager.xml',
        'views/schedule_template.xml',
        'views/auto-schedule.xml',

    ],

    'qweb': [
        "static/src/xml/schedule.xml",
    ],
}
