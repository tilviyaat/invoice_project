# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Limits(models.Model):
    _inherit = 'res.users'
    users_limit = fields.Float(string='User limit')

    @api.onchange('branch_id')
    def Default(self):
        branch_val = self.env['res.branch'].search([("name", "=", self.branch_id.name)])
        for i in branch_val:
             print(i.cost)
             self.branch_id.cost = i.cost
             self.users_limit = i.cost



class Branch(models.Model):
    _inherit = "res.branch"
    cost = fields.Float(string='Cost')

    @api.onchange('cost')
    def default(self):
        user_list = self.env['res.users'].search([("branch_id.name", "=", self.name)])
        for i in user_list:
            i.users_limit = self.cost



class Schedule(models.Model):
    _inherit = "mail.activity.type"
    category = fields.Selection(selection_add=[
        ('approve','Approved'),
        ('reject','Rejected')
    ])


class Approve(models.Model):
    _inherit = "mail.activity"

    def schedule_reject(self):
        obj = self.env[self.res_model].search([('id', '=', self.res_id)])
        print(self)
        obj.state = "rejected"

    def schedule_approval(self):
        obj = self.env[self.res_model].search([('id', '=', self.res_id)])
        print(self)
        obj.state = "approved"


