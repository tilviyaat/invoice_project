from odoo import models, fields, api,_
from odoo.http import request


class Invoice_approval(models.Model):
    schedule_manager = fields.Many2one('res.users', string='Schedule_Manager')
    _inherit = "account.move"
    state = fields.Selection([

        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('posted','Posted'),
        # ('rfa','Waiting for approval'),
        ('overlimit','Request for approval'),
        ('wfa','Waiting for approval')

    ], string='Status', readonly=True, default='draft')

    def waiting_approval(self):
        self.state = 'wfa'


    def request_approve(self):
        self.state = 'approved'
        for rec in self:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (rec.id, rec._name)
            body_html = _("""Quotation %s has been approved by %s<br>
                               <p style="margin: 16px 0px 16px 0px;">
                               <a href="%s" target="_blank"
                               style="background-color:#875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px;">
                               View Quotation
                               </a></p>""") % \
                        (rec.name, rec.env.user.name, base_url)
            subtype_id = self.env['mail.message.subtype'].search([('name', '=', "Discussions")])
            subtype_log = self.env['mail.message.subtype'].search([('name', '=', "Note")])

        self.env['mail.mail'].create({
            'body_html': body_html,
            'state': 'outgoing',
            'email_to': str(rec.user_id.partner_id.email),
            'subject': "Manager Approved"
        }).send()


    def request_confirm(self):
        current_user = self.env['res.users'].search([("name", "=", self.env.user.name)])
        total_amount = self.amount_total

        for i in current_user:
            if total_amount <= i.users_limit:
                print(i.users_limit)
                self.state = 'confirm'
            else:
                self.state = 'overlimit'


    def request_rejected(self):
        self.state = 'rejected'
        print(self)
        for rec in self:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (rec.id, rec._name)
            body_html = _("""Quotation %s has been rejected by %s<br>
                        <p style="margin: 16px 0px 16px 0px;">
                        <a href="%s" target="_blank"
                        style="background-color:#875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px;">
                        View Quotation
                        </a></p>""") % \
                        (rec.name, rec.env.user.name, base_url)
            subtype_id = self.env['mail.message.subtype'].search([('name', '=', "Discussions")])
            subtype_log = self.env['mail.message.subtype'].search([('name', '=', "Note")])

        self.env['mail.mail'].create({
            'body_html': body_html,
            'state': 'outgoing',
            'email_to': str(rec.user_id.partner_id.email),
            'subject': "Manager Rejected"
        }).send()
    def request_cancel(self):
        self.state = 'cancel'
    def action_post(self):
        self.state = 'posted'

    def request_approval(self):
        self.activity_update()
        # self.state = 'wfa'
        in_manager = self.env['hr.employee'].search([('name','=',self.env.user.name)])
        self.manager_name = in_manager.parent_id.name
        for rec in self:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (rec.id, rec._name)
            body_html = _("""Quotation %s has been requested  by %s<br>
                                   <p style="margin: 16px 0px 16px 0px;">
                                   <a href="%s" target="_blank"
                                   style="background-color:#875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px;">
                                   View Quotation
                                   </a></p>""") % \
                        (rec.name, rec.env.user.name, base_url)
            self.env['mail.mail'].create({
                'body_html': body_html,
                'state': 'outgoing',
                'email_to': str(in_manager.parent_id.work_email),
                'subject': "Request for approval"
            }).send()

            rec.write({'state': 'wfa'})


    def request_manager(self):
        # invoice_manager = self.env['hr.employee'].search([('name', '=', self.invoice_user_id.name)])
        # current_user = self.env['hr.employee'].search([('name', '=', self.env.user.name)])

                if self.env.user.id == self.invoice_user_id.id:
                    self.manager = True

                elif self.env.user.name == self.manager_name:
                    self.manager = True

                else:
                    self.manager = False
    manager = fields.Boolean(string='manager', compute='request_manager')
    manager_name = fields.Char(string='manager name')

    def _get_responsible_for_approval(self):
        if self.user_id:
            return self.user_id
        elif self.user.name.parent_id.user_id:
            return self.user.name.parent_id.user_id
        elif self.user.name.department_id.manager_id.user_id:
            return self.user.name.department_id.manager_id.user_id
        return self.env['res.users']

    def activity_update(self):
        print("kk")
        for expense_report in self.filtered(lambda hol: hol.state == 'overlimit'):
            self.activity_schedule(
                'invoice_project.mail_activity_approve',
                    user_id = expense_report.sudo()._get_responsible_for_approval().id or self.env.user.id)
        # self.filtered(lambda hol: hol.state == 'approved').activity_feedback(['invoice_project.mail_activity_approve'])
        # self.filtered(lambda hol: hol.state == 'cancel').activity_unlink(['account.mail_activity_approve'])
